### Created by _Alessandro Muzzi, alessandro.muzzi@studenti.unipr.it_
## Sistemi Distribuiti, Assignment 3

This project represents contains two main classes:
* **it.muzzialessandro.Client**: represents a client asking and operating on shared resources
* **it.muzzialessandro.Coorindator**: represents a coordinator that manages the access to the shared resources 

Each of these classes must be launched multiple times depending on the network structure.

Each class has a configuration file called respectively **client_config.xml** and **coordinator_config.xml** situated in _src/main/resources/_ which must be passed as the only argument launching the processes.

The node doesn't have a "stop" command, so it must be killed once its use it's finished.
