package it.muzzialessandro.actor;

import com.opencsv.bean.CsvToBeanBuilder;
import it.muzzialessandro.manager.ErrorManager;
import it.muzzialessandro.manager.LogManager;
import it.muzzialessandro.model.Company;
import it.unipr.sowide.actodes.actor.Behavior;
import it.unipr.sowide.actodes.actor.CaseFactory;
import it.unipr.sowide.actodes.actor.Shutdown;
import it.unipr.sowide.actodes.configuration.Configuration;
import it.unipr.sowide.actodes.controller.SpaceInfo;
import it.unipr.sowide.actodes.examples.messaging.Sender;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.lang.Runtime.getRuntime;

/**
 * The {@code Receiver} class defines a behavior that waits for a specific
 * number of messages from a {@code Sender} actor and then kills itself.
 *
 * @see Sender
 **/

public final class SlaveProcess extends Behavior {

    private static final long serialVersionUID = 1L;

    //region Private Fields

    // Actor ID
    private final int id;
    // Number of slaves
    private final int slaveNumber;
    // Companies
    private List<Company> companies;
    // Configuration
    private final HashMap<String, Object> config;
    // Bounds for this client
    private int companiesStartIdx;
    private int companiesEndIdx;
    private int companiesAmount;

    //endregion

    public SlaveProcess(final int id, final HashMap<String, Object> config) {
        this.id = id;
        this.config = config;
        this.slaveNumber = (int) config.get("slavesNumber");
    }

    private void init() {
        this.companies = importCompaniesWithPrices();

        // calculate the bounds for this slave
        int companiesSize = this.companies.size();
        this.companiesAmount = (int) Math.floor((float) companiesSize / this.slaveNumber);
        this.companiesStartIdx = this.companiesAmount * this.id;
        if(this.id == this.slaveNumber - 1)
            this.companiesAmount = companiesSize - this.companiesStartIdx;
        this.companiesEndIdx = companiesStartIdx + this.companiesAmount;

        LogManager.i("Companies from " + this.companiesStartIdx + " to " + this.companiesEndIdx);
        LogManager.i("Companies amount: " + this.companiesAmount);
    }

    /**
     * {@inheritDoc}
     **/
    @Override
    public void cases(final CaseFactory c) {

        c.define(START, (m) -> {
            LogManager.i("Slave started");
            init();

            return null;
        });

        c.define(MessagePatterns.START_PROCESSING_DATA, (m) -> {

            int startYear = (int) this.config.get("startYear");
            int endYear = (int) this.config.get("endYear");
            int correlationWindow = (int) this.config.get("correlationWindow");

            // calculate Pardalos scores
            ZoneId localZone = ZoneId.systemDefault();
            LocalDate fromDate = new GregorianCalendar(startYear, Calendar.JANUARY, 1).getTime().toInstant().atZone(localZone).toLocalDate();
            LocalDate toDate = new GregorianCalendar(endYear, Calendar.DECEMBER, 31).getTime().toInstant().atZone(localZone).toLocalDate();

            long startTime = System.nanoTime();
            int threadsNumber = getRuntime().availableProcessors();
            List<Thread> threads = new ArrayList<>();
            int companiesAmountPerNode = this.companiesAmount;
            int companiesAmountPerThread = (int) Math.floor((float) companiesAmountPerNode / threadsNumber);

            // starting all the threads
            for (int i = 0; i< threadsNumber; i++) {
                int startIdx = companiesAmountPerThread * i;
                if(i == threadsNumber - 1)
                    companiesAmountPerThread = companiesAmountPerNode - startIdx;
                int endIdx = startIdx + companiesAmountPerThread;

                Thread t = new Thread(() -> processPartialData(this.companiesStartIdx + startIdx, this.companiesStartIdx + endIdx, fromDate, toDate, correlationWindow));
                threads.add(t);
                t.start();
            }
            threads.forEach(it -> {
                try {
                    it.join();
                } catch (InterruptedException e) {
                    ErrorManager.raiseError(3, "Error occurred while waiting for a thread", e);
                }
            });
            long stopTime = System.nanoTime();
            LogManager.i("Pardalos scores in " + (stopTime - startTime)/1000 + "us");

            send(m, new ArrayList<>(this.companies.subList(this.companiesStartIdx, this.companiesEndIdx)));

            return Shutdown.SHUTDOWN;
        });
    }

    //region Inner Methods

    private List<Company> importCompaniesWithPrices() {
        Configuration conf = SpaceInfo.INFO.getConfiguration();

        String fileName = conf.getString("csvFileName");
        String dataFolderPath = conf.getString("dataFolderPath");

        List<Company> companiesWithoutPrices = null;
        List<Company> companiesWithPrices = new LinkedList<>();
        try {
            companiesWithoutPrices = new CsvToBeanBuilder(new FileReader(fileName))
                    .withSkipLines(1)
                    .withType(Company.class)
                    .build()
                    .parse();

            // import prices
            LogManager.i("Importing companies prices");
            int i;
            for (i = 0; i < companiesWithoutPrices.size(); i++) {
                if(i % 200 == 0 && i > 0)
                    LogManager.i(i + " companies prices imported");

                Company company = companiesWithoutPrices.get(i);
                if (Files.exists(Path.of(dataFolderPath + "/" + company.getSymbol() + ".csv"))) {
                    BufferedReader reader = new BufferedReader(new FileReader(dataFolderPath + "/" + company.getSymbol() + ".csv"));
                    try {
                        company.importPricesFromReader(reader, null);
                        companiesWithPrices.add(company);
                        reader.close();
                    } catch (IOException | ParseException e) {
                        LogManager.i("Error importing prices for " + company.getSymbol() + ": " + e.getMessage());
                    }
                }
            }
            LogManager.i(i + " companies prices imported");
        }
        catch (FileNotFoundException e) {
            ErrorManager.raiseError(4, "Error while importing companies prices.", e);
        }

        return companiesWithPrices;
    }

    void processPartialData(int startIdx, int endIdx, LocalDate fromDate, LocalDate toDate, int correlationWindow){
        DateTimeFormatter dt = DateTimeFormatter.ofPattern("yyyy-MM");

        for(int i = startIdx; i < endIdx; i++) {
            Company comp1 = this.companies.get(i);

            LogManager.i("#" + i + " Calculating Pardalos scores for " + comp1.getSymbol());
            HashMap<String, HashMap<String, Double>> comp1Correlations = new HashMap<>();

            LocalDate tmpFromDate = fromDate;
            LocalDate tmpToDate = fromDate.plusMonths(correlationWindow);
            // while tmpToDate <= toDate
            while (!tmpToDate.isAfter(toDate)) {
                HashMap<String, Double> currentCorrelations = new HashMap<>();
                for(Company comp2 : this.companies) {
                    if (comp1 != comp2) {
                        currentCorrelations.put(comp2.getSymbol(), getPardalosScore(comp1, comp2, tmpFromDate, tmpToDate));
                    }
                }
                // add the just calculated correlations
                comp1Correlations.put(tmpFromDate.format(dt) + "_" + tmpToDate.format(dt), currentCorrelations);
                // go to the next correlation window
                tmpFromDate = tmpToDate;
                tmpToDate = tmpToDate.plusMonths(correlationWindow);
            }

            comp1.setCorrelations(comp1Correlations);
        }
    }

    double getPardalosScore(@NotNull Company c1, @NotNull Company c2, LocalDate startDate, LocalDate endDate) {
        // try to find an interval in which both companies have prices
        LocalDate c1StartDate = c1.getBoundedDate(startDate);
        LocalDate c1EndDate = c1.getBoundedDate(endDate);
        LocalDate c2StartDate = c2.getBoundedDate(startDate);
        LocalDate c2EndDate = c2.getBoundedDate(endDate);
        LocalDate startD = c1StartDate;
        LocalDate endD = c2EndDate;

        if(c1StartDate.isBefore(c2StartDate))
            startD = c2StartDate;
        if(c1EndDate.isBefore(c2EndDate))
            endD = c1EndDate;

        if(startD.equals(endD) || startD.isAfter(endD) || endD.isBefore(startD))
            return 0;

        // try to match start/end dates for both companies, taking care of eventual missing dates
        Company.PricesForDates pricesC1 = c1.getAdjClosePricesForPeriod(startD, endD);
        if(pricesC1 == null) {
            return 0;
        }
        Company.PricesForDates pricesC2 = c2.getAdjClosePricesForPeriod(pricesC1.getStartDate(), pricesC1.getEndDate());
        if(pricesC2 == null) {
            return 0;
        }

        if(pricesC1.getStartDate().equals(pricesC2.getStartDate()) || pricesC1.getEndDate().equals(pricesC2.getEndDate()))
            pricesC1 = c1.getAdjClosePricesForPeriod(pricesC2.getStartDate(), pricesC2.getEndDate());

        if(pricesC1 == null || pricesC1.getPrices().size() != pricesC2.getPrices().size()) {
            return 0;
        }

        LinkedList<Float> pricesC1ROS = ROS(pricesC1.getPrices());
        LinkedList<Float> pricesC2ROS = ROS(pricesC2.getPrices());

        // Numerator
        float correlationNumerator = dotProduct(pricesC1ROS, pricesC2ROS) - dotProduct(pricesC1ROS) * dotProduct(pricesC2ROS);

        LinkedList<Float> pricesC1ROSSquared = powArray(pricesC1ROS, 2);
        LinkedList<Float> pricesC2ROSSquared = powArray(pricesC2ROS, 2);
        float term1 = dotProduct(subtractFromArray(pricesC1ROSSquared, (float) Math.pow(dotProduct(pricesC1ROS), 2)));
        float term2 = dotProduct(subtractFromArray(pricesC2ROSSquared, (float) Math.pow(dotProduct(pricesC2ROS), 2)));

        double correlationDenominator = Math.sqrt(term1*term2);

        if(correlationDenominator == 0)
            return 0;

        return (correlationNumerator / correlationDenominator) / pricesC1ROS.size();
    }

    /**
     * Gets the Return Of the Stock for each element of the given prices by performing
     * ln({@code prices}[i]/{@code prices}[i - 1]) for each i.
     * It worth noting that the resulting array will have length prices.length - 1
     * @param prices    The prices of the stock
     * @return          The return of the stock for day t
     */
    LinkedList<Float> ROS(LinkedList<Float> prices) {
        LinkedList<Float> ros = new LinkedList<>();

        for(int i = 1; i < prices.size(); i++) {
            ros.add((float) Math.log(prices.get(i)/prices.get(i - 1)));
        }

        return ros;
    }

    /**
     * Calculates the dot product of the given vector with its own
     * @param prices1   The vector
     * @return          The dot product of the vectors
     */
    float dotProduct(LinkedList<Float> prices1) {
        return dotProduct(prices1, prices1);
    }

    /**
     * Calculates the dot product of the given vectors
     * @param prices1   The first vector
     * @param prices2   The second vector
     * @return          The dot product of the vectors
     */
    float dotProduct(LinkedList<Float> prices1, LinkedList<Float> prices2) {
        float sum = 0.0f;
        for(int i = 0; i < prices1.size(); i++) {
            sum += prices1.get(i) * prices2.get(i);
        }

        return sum;
    }

    LinkedList<Float> powArray(LinkedList<Float> array, int powExponent) {
        LinkedList<Float> pows = new LinkedList<>();

        for (Float item : array) {
            pows.add((float) Math.pow(item, powExponent));
        }

        return pows;
    }

    LinkedList<Float> subtractFromArray(LinkedList<Float> array, float number) {
        LinkedList<Float> res = new LinkedList<>();

        for (Float item : array) {
            res.add(item - number);
        }

        return res;
    }

    //endregion
}
