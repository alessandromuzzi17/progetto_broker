package it.muzzialessandro.actor;

import it.unipr.sowide.actodes.actor.MessagePattern;
import it.unipr.sowide.actodes.filtering.RegexMatcher;
import it.unipr.sowide.actodes.filtering.RegexPattern;
import it.unipr.sowide.actodes.filtering.constraint.Matches;

/**
 * Custom message patterns
 */
public class MessagePatterns {

    public static String START_RETRIEVING_PRICES_PATTERN = "start_retrieving_prices";
    public static MessagePattern START_RETRIEVING_PRICES = MessagePattern.contentPattern(
            new Matches<>(new RegexPattern(START_RETRIEVING_PRICES_PATTERN), RegexMatcher.INSTANCE));

    public static String START_PROCESSING_DATA_PATTERN = "start_processing_data";
    public static MessagePattern START_PROCESSING_DATA = MessagePattern.contentPattern(
            new Matches<>(new RegexPattern(START_PROCESSING_DATA_PATTERN), RegexMatcher.INSTANCE));
}
