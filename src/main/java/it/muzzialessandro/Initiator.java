package it.muzzialessandro;

import com.opencsv.bean.CsvToBeanBuilder;
import it.muzzialessandro.actor.MessagePatterns;
import it.muzzialessandro.actor.SlaveProcess;
import it.muzzialessandro.actor.SlaveScan;
import it.muzzialessandro.manager.ErrorManager;
import it.muzzialessandro.manager.LogManager;
import it.muzzialessandro.model.Company;
import it.unipr.sowide.actodes.actor.Behavior;
import it.unipr.sowide.actodes.actor.CaseFactory;
import it.unipr.sowide.actodes.actor.MessageHandler;
import it.unipr.sowide.actodes.actor.Shutdown;
import it.unipr.sowide.actodes.configuration.Configuration;
import it.unipr.sowide.actodes.controller.Controller;
import it.unipr.sowide.actodes.controller.SpaceInfo;
import it.unipr.sowide.actodes.distribution.activemq.ActiveMqConnector;
import it.unipr.sowide.actodes.executor.active.ThreadCoordinator;
import it.unipr.sowide.actodes.interaction.Create;
import it.unipr.sowide.actodes.registry.Reference;
import it.unipr.sowide.actodes.service.logging.ConsoleWriter;
import it.unipr.sowide.actodes.service.logging.Logger;
import it.unipr.sowide.actodes.service.logging.util.NoCycleProcessing;
import org.gephi.appearance.api.AppearanceController;
import org.gephi.appearance.api.AppearanceModel;
import org.gephi.appearance.api.Function;
import org.gephi.appearance.plugin.RankingElementColorTransformer;
import org.gephi.appearance.plugin.RankingNodeSizeTransformer;
import org.gephi.filters.api.FilterController;
import org.gephi.filters.api.Query;
import org.gephi.filters.api.Range;
import org.gephi.filters.plugin.graph.DegreeRangeBuilder;
import org.gephi.graph.api.*;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.EdgeDirectionDefault;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.layout.plugin.force.yifanHu.YifanHuLayout;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.types.DependantColor;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.preview.types.EdgeColor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.statistics.plugin.GraphDistance;
import org.openide.util.Lookup;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.List;
import java.util.*;

/**
 * The {@code Initiator} class defines a behavior that creates
 * a buffer actor and a set of {@code Producer} and {@code Consumer} actors.
 * <p>
 * After a fixed period of time it asks all the actors to kill themselves.
 * <p>
 * Note that the buffer actor can use two types of behavior:
 * one based on behavior change and the other based on state change.
 * <p>
 * This class allows to a user to select the type of behavior,
 * the size of the buffer, the number of producers and consumers
 * and the length of the execution.
 **/

public final class Initiator extends Behavior {

    private static final long serialVersionUID = 1L;

    private enum WorkingMode {
        Scan,
        Process,
        View
    }

    //region Private Fields

    private Reference[] references;
    private int index;
    private int slavesNumber;
    private int completedJobSlaves;
    private WorkingMode mode;
    private HashMap<String, Object> config;

    private List<Company> companies;

    //endregion


    /**
     * Class constructor.
     * <p>
     * the implementation flag: <code>true</code> for the behavior
     * change implementation" and <code>false</code> for the state
     * change implementation.
     **/
    public Initiator(WorkingMode mode, HashMap<String, Object> config) throws FileNotFoundException {
        this.mode = mode;
        this.config = config;
        this.index = 0;
        this.slavesNumber = 0;
        this.completedJobSlaves = 0;
        this.companies = new ArrayList<>();

        if(mode == WorkingMode.View) {
            exportGraphsAsPDF();
            LogManager.i("PDF generation completed");
            System.exit(0);
        }
    }


    /**
     * {@inheritDoc}
     **/
    @Override
    public void cases(final CaseFactory caseFactory) {
        caseFactory.define(START, (MessageHandler) (m) -> {
            LogManager.i("start");

            int providersNumber = SpaceInfo.INFO.getProviders().size();
            if (providersNumber > 0) {
                this.references = new Reference[providersNumber];
                config.put("slavesNumber", providersNumber - 1);

                MessageHandler c1 = (n) -> {
                    Reference ref = (Reference) n.getContent();
                    this.references[this.index++] = ref;

                    future(ref, MessagePatterns.START_RETRIEVING_PRICES_PATTERN, (MessageHandler) o -> {

                        System.out.println("Received companies from a slave");
                        this.completedJobSlaves++;

                        this.companies.addAll((ArrayList<Company>) o.getContent());

                        if(this.slavesNumber == this.completedJobSlaves)
                            return Shutdown.SHUTDOWN;

                        return null;
                    });

                    return null;
                };

                MessageHandler c2 = (n) -> {
                    Reference ref = (Reference) n.getContent();
                    this.references[this.index++] = ref;

                    future(ref, MessagePatterns.START_PROCESSING_DATA_PATTERN, (MessageHandler) o -> {

                        System.out.println("Received Pardalos scores from a slave");
                        this.completedJobSlaves++;

                        this.companies.addAll((ArrayList<Company>) o.getContent());

                        if(this.slavesNumber == this.completedJobSlaves) {
//                            createGraphs();
                            serializeGraphs();
                            return Shutdown.SHUTDOWN;
                        }

                        return null;
                    });

                    return null;
                };

                Set<Reference> providers = SpaceInfo.INFO.getProviders();
                Reference broker = retrieveBroker();
                providers.forEach(provider -> {
                    if (!provider.equals(broker)) {
                        if (this.mode == WorkingMode.Scan) {
                            // passing to the slave its ID and the total count of the providers minus the broker
                            future(provider, new Create(new SlaveScan(this.slavesNumber, config)), c1);
                        }
                        else if (this.mode == WorkingMode.Process) {
                            // passing to the slave its ID and the total count of the providers minus the broker
                            future(provider, new Create(new SlaveProcess(this.slavesNumber, config)), c2);
                        }

                        this.slavesNumber++;
                    }
                });
            }

            return null;
        });
    }

    private void serializeGraphs() {
        double threshold = (double) this.config.get("threshold");
        String outputPath = (String) this.config.get("outputPath");

        Path outputDir = Path.of(outputPath);
        if(!Files.exists(outputDir)) {
            try {
                Files.createDirectory(outputDir);
            } catch (IOException e) {
                ErrorManager.raiseError(10, "Can't create output directory for graph files", e);
            }
        }

        String serializedGraph = "graph [\n\tdirected 0";
        int i = 0;
        // adding companies (aka nodes) to the file
        HashMap<String, Integer> nodes = new HashMap<>();
        for(Company company : this.companies) {
            i++;
            nodes.put(company.getSymbol(), i);
            serializedGraph += "\n\tnode [" +
                    "\n\t\tid " + i +
                    "\n\t\tlabel \"" + company.getSymbol() + "\"" +
                    "\n\t\tvalue " + 0 +
                    "\n\t\tsource \"" + company.getName() + "\"" +
                "\n\t]";
        }

        // adding correlations (aka edges)
        for(String corrKey : this.companies.get(0).getCorrelations().keySet()) {
            String currentSerializedGraph = serializedGraph;
            LogManager.i("Processing correlation window: " + corrKey);

            for(Company company : this.companies) {
                LogManager.i("\tProcessing company: " + company.getSymbol());

                // create the company link to other companies
                int edgesCount = 0;
                for (var entry : company.getCorrelations().get(corrKey).entrySet()) {
                    if(entry.getValue() >= threshold) {
                        edgesCount++;
                        currentSerializedGraph += "\n\tedge [" +
                                "\n\t\tsource " + nodes.get(company.getSymbol()) +
                                "\n\t\ttarget " + nodes.get(entry.getKey()) +
                                "\n\t]";
                    }
                }

                //See if graph is well imported

                LogManager.i("Current graph has #Nodes: " + this.companies.size());
                LogManager.i("Current graph has #Edges: " + edgesCount);

            }

            currentSerializedGraph += "\n]";

            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(outputPath + "/correlation_graph_" + corrKey + ".gml"));
                writer.write(currentSerializedGraph);

                writer.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void exportGraphsAsPDF() {
        String gmlFilePath = (String) this.config.get("gmlFilePath");
        String outputPath = (String) this.config.get("outputPath");

        // add requested file/s to processing queue
        List<String> gmlFiles = new ArrayList<>();
        File gmlFile = new File(gmlFilePath);
        if(!gmlFile.exists()) {
            ErrorManager.raiseError(9, "Invalid gml file/folder path provided!");
        }

        Path outputDir = Path.of(outputPath);
        if(!Files.exists(outputDir)) {
            try {
                Files.createDirectory(outputDir);
            } catch (IOException e) {
                ErrorManager.raiseError(11, "Can't create output directory for PDF files", e);
            }
        }

        if(gmlFile.isFile())
            gmlFiles.add(gmlFilePath);
        else {
            File[] listOfFiles = gmlFile.listFiles();

            assert listOfFiles != null;
            for (File folderElement : listOfFiles) {
                if (folderElement.isFile() && folderElement.getPath().endsWith(".gml")) {
                    gmlFiles.add(folderElement.getPath());
                }
            }
        }

        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        for(String gmlPath : gmlFiles) {
            LogManager.i("Processing " + gmlPath);
            //Init a project - and therefore a workspace
            pc.newProject();
            Workspace workspace = pc.getCurrentWorkspace();

            //Get models and controllers for this new workspace - will be useful later
            GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
            PreviewModel model = Lookup.getDefault().lookup(PreviewController.class).getModel();
            ImportController importController = Lookup.getDefault().lookup(ImportController.class);
            FilterController filterController = Lookup.getDefault().lookup(FilterController.class);
            AppearanceController appearanceController = Lookup.getDefault().lookup(AppearanceController.class);
            AppearanceModel appearanceModel = appearanceController.getModel();

            //Import file
            Container container;
            try {
                File file = new File(gmlPath);
                container = importController.importFile(file);
                container.getLoader().setEdgeDefault(EdgeDirectionDefault.DIRECTED);   //Force DIRECTED
            } catch (Exception ex) {
                ex.printStackTrace();
                return;
            }

            //Append imported data to GraphAPI
            importController.process(container, new DefaultProcessor(), workspace);

            //See if graph is well imported
            DirectedGraph graph = graphModel.getDirectedGraph();

            //Filter
            DegreeRangeBuilder.DegreeRangeFilter degreeFilter = new DegreeRangeBuilder.DegreeRangeFilter();
            degreeFilter.init(graph);
            degreeFilter.setRange(new Range(30, Integer.MAX_VALUE));     //Remove nodes with degree < 30
            Query query = filterController.createQuery(degreeFilter);
            GraphView view = filterController.filter(query);
            graphModel.setVisibleView(view);    //Set the filter result as the visible view

            //See visible graph stats
//            UndirectedGraph graphVisible = graphModel.getUndirectedGraphVisible();

            //Run YifanHuLayout for 100 passes - The layout always takes the current visible view
            YifanHuLayout layout = new YifanHuLayout(null, new StepDisplacement(1f));
            layout.setGraphModel(graphModel);
            layout.resetPropertiesValues();
            layout.setOptimalDistance(400f);

            layout.initAlgo();
            for (int i = 0; i < 100 && layout.canAlgo(); i++) {
                layout.goAlgo();
            }
            layout.endAlgo();

            //Get Centrality
            GraphDistance distance = new GraphDistance();
            distance.setDirected(true);
            distance.execute(graphModel);

            //Rank color by Degree
            Function degreeRanking = appearanceModel.getNodeFunction(graph, AppearanceModel.GraphFunction.NODE_DEGREE, RankingElementColorTransformer.class);
            RankingElementColorTransformer degreeTransformer = degreeRanking.getTransformer();
            degreeTransformer.setColors(new Color[]{new Color(0xFEF0D9), new Color(0xB30000)});
            degreeTransformer.setColorPositions(new float[]{0f, 1f});
            appearanceController.transform(degreeRanking);

            //Rank size by centrality
            Column centralityColumn = graphModel.getNodeTable().getColumn(GraphDistance.BETWEENNESS);
            Function centralityRanking = appearanceModel.getNodeFunction(graph, centralityColumn, RankingNodeSizeTransformer.class);
            if(centralityRanking == null) {
                LogManager.i("Skipped current graph, too few edges!");
                continue;
            }
            RankingNodeSizeTransformer centralityTransformer = centralityRanking.getTransformer();
            centralityTransformer.setMinSize(2);
            centralityTransformer.setMaxSize(4);
            appearanceController.transform(centralityRanking);

            //Preview
            model.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
            model.getProperties().putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(Color.GRAY));
            model.getProperties().putValue(PreviewProperty.EDGE_THICKNESS, 0.05f);
            model.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT, model.getProperties().getFontValue(PreviewProperty.NODE_LABEL_FONT).deriveFont(Font.ITALIC));

            //Export
            ExportController ec = Lookup.getDefault().lookup(ExportController.class);
            try {
                ec.exportFile(new File(outputPath + "/" + gmlPath.substring(gmlPath.lastIndexOf("/"), gmlPath.lastIndexOf(".")) + ".pdf"));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Retrieve the broker ensuring that it is the right one
     * @return The broker
     */
    private Reference retrieveBroker() {
        Reference broker;
        // busy wait to fix the race condition between the ask for the broker here
        // and the set of the actual broker after the connection has been established
        while ((broker = Controller.CONTROLLER.getDispatcher().getBroker()) == null) {
            try {
                //noinspection BusyWait
                Thread.sleep(5);
            } catch (InterruptedException e) {
                //ignore
            }
        }

        return broker;
    }

    /**
     * Starts an actor space running the buffer example.
     *
     * @param v the arguments.
     *          <p>
     *          It does not need arguments.
     **/
    public static void main(final String[] v) {
        try {
            Configuration conf = SpaceInfo.INFO.getConfiguration();

            if (!conf.load(v)) {
                ErrorManager.raiseError(1, "Config file not found");
                return;
            }

            String csvFileName = conf.getString("csvFileName");
            final String brokerIP = conf.getString("broker_ip");

            conf.setFilter(Logger.ACTIONS);
            conf.setLogFilter(new NoCycleProcessing());
            conf.addWriter(new ConsoleWriter());

            Scanner scanner = new Scanner(System.in);
            boolean stopInput = false;

            do {
                System.out.println("Enter:");
                System.out.println(" s <start year> <end year> <data frequency (one of [\"1d\" daily, \"1wk\" weekly, \"1mo\" monthly])>\n\tfor companies prices scan and retrieval (Eg. s 2000 2020 1d)");
                System.out.println(" p <start year> <end year> <correlation window (in months)> <Pearson correlation threshold> <output folder path for the gml files>\n\tfor already downloaded data processing (Eg. p 2000 2020 4 0.9 ./output)");
                System.out.println(" v <path of the gml file/folder> <output path for the .pdf files>\n\tto generate pdf graphs from already downloaded data, in case the parameter is a file, a single pdf will be generated, otherwise all the .gml files under the specified path will be processed");
                System.out.println(" e \n\tfor exit");

                String[] command = scanner.nextLine().split(" ");
                // TODO UNCOMMENT TO LAUNCH WITHOUT COMMAND PROMPT (NOHUP CASE)
//                String[] command = List.of("p", "2000", "2020", "4", ".9", "gmlFiles").toArray(new String[0]);

                HashMap<String, Object> config = new HashMap<>();
                switch (command[0]) {
                    case "s":
                        config.put("startYear", Integer.parseInt(command[1]));
                        config.put("endYear", Integer.parseInt(command[2]));
                        config.put("dataFrequency", command[3]);
                        conf.setExecutor(new ThreadCoordinator(new Initiator(WorkingMode.Scan, config)));
                        stopInput = true;
                        break;
                    case "p":
                        config.put("startYear", Integer.parseInt(command[1]));
                        config.put("endYear", Integer.parseInt(command[2]));
                        config.put("correlationWindow", Integer.parseInt(command[3]));
                        config.put("threshold", Double.parseDouble(command[4]));
                        config.put("outputPath", command[5]);
                        conf.setExecutor(new ThreadCoordinator(new Initiator(WorkingMode.Process, config)));
                        stopInput = true;
                        break;
                    case "v":
                        config.put("gmlFilePath", command[1]);
                        config.put("outputPath", command[2]);
                        conf.setExecutor(new ThreadCoordinator(new Initiator(WorkingMode.View, config)));
                        stopInput = true;
                        break;
                    case "e":
                        return;
                }
            } while (!stopInput);
            scanner.close();
            conf.setConnector(new ActiveMqConnector("tcp://" + brokerIP + ":61616"));
            LogManager.i("Starting ActoDES Initiator");

            conf.start();
        } catch (FileNotFoundException e) {
            ErrorManager.raiseError(2, "Unable to start ActoDES application.", e);
        }

    }
}
