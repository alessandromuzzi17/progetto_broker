package it.muzzialessandro.utils;

import it.muzzialessandro.manager.ErrorManager;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NetworkUtils {

    public static String getDefaultLocalAddress() throws SocketException {
        Enumeration<NetworkInterface> iter = NetworkInterface.getNetworkInterfaces();
        InetAddress localAddr = null;
        while (iter.hasMoreElements() && localAddr == null) {
            NetworkInterface i = iter.nextElement();
            if(!i.getName().startsWith("vm")) {
                Enumeration<InetAddress> inetAddresses = i.getInetAddresses();
                while (inetAddresses.hasMoreElements() && localAddr == null) {
                    InetAddress addr = inetAddresses.nextElement();
                    if(!addr.isLoopbackAddress() && addr instanceof Inet4Address)
                        localAddr = addr;
                }
            }
        }

        if(localAddr == null){
            ErrorManager.raiseError(1, "Can't find a valid local address.");
        }

        assert localAddr != null;
        return localAddr.getHostAddress();
    }

}
