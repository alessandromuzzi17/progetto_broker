package it.muzzialessandro.utils;

public class RangePair {
    private int min;
    private int max;

    public RangePair(int min, int max) {
        this.min = min;
        this.max = max;
    }

    //region Getter and Setters

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }


    //endregion

    public int gap() {
        return this.max - this.min;
    }
}
