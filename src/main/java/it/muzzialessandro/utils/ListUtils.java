package it.muzzialessandro.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

    public static <T> List<List<T>> partition(List<T> list, int numberOfParts) {
        List<List<T>> parts = new ArrayList<>();

        int itemsPerPart = (int)Math.ceil((double) list.size() / numberOfParts);
        for(int i = 0; i < numberOfParts; i++) {
            parts.add(list.subList(i * itemsPerPart, Math.min((i+1) * itemsPerPart, list.size())));
        }

        return parts;
    }

}
