package it.muzzialessandro;

import com.opencsv.bean.CsvToBeanBuilder;
import it.muzzialessandro.manager.LogManager;
import it.muzzialessandro.model.Company;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class Main {



    public static void main(String[] args) throws IOException {

        String fileName = "./src/main/resources/us_market.csv";

        List<Company> beans = new CsvToBeanBuilder(new FileReader(fileName))
                .withType(Company.class)
                .build()
                .parse();

        // import prices
        List<Company> companies = new ArrayList<>();
        int i;
        for (i = 1; i < 3000; i++) {
            if(i % 200 == 0 && i > 0)
                LogManager.i(i + " companies prices imported");

            Company company = beans.get(i);
            if(Files.exists(Path.of("./src/main/resources/data/" + company.getSymbol() + ".csv"))) {
                BufferedReader reader = new BufferedReader(new FileReader("./src/main/resources/data/" + company.getSymbol() + ".csv"));
                try {
                    company.importPricesFromReader(reader, null);
                    companies.add(company);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        LogManager.i(i + " companies prices imported");

        LogManager.i("All companies imported");
        // calculate pardalos scores
        ZoneId localZone = ZoneId.systemDefault();
        LocalDate fromDate = new GregorianCalendar(2008, Calendar.FEBRUARY, 1).getTime().toInstant().atZone(localZone).toLocalDate();
        LocalDate toDate = new GregorianCalendar(2008, Calendar.MAY, 1).getTime().toInstant().atZone(localZone).toLocalDate();
        long startTime = System.nanoTime();

        // TODO ADD CHARTS

        double[] correlations = new double[companies.size() - 1];
        Company company0 = companies.get(0);
        for(i = 1; i < companies.size(); i++) {
            correlations[i-1] = getPardalosScore(company0, companies.get(i), fromDate, toDate);
            LogManager.i("Correlation " + company0.getSymbol() + "-" + companies.get(i).getSymbol() + " is: " + correlations[i-1]);
        }
        long stopTime = System.nanoTime();
        LogManager.i("Pardalos scores in " + (stopTime - startTime)/1000 + "us");
    }

    static double getPardalosScore(@NotNull Company c1, @NotNull Company c2, LocalDate startDate, LocalDate endDate) {
        // try to find an interval in which both companies have prices
        LocalDate c1StartDate = c1.getBoundedDate(startDate);
        LocalDate c1EndDate = c1.getBoundedDate(endDate);
        LocalDate c2StartDate = c2.getBoundedDate(startDate);
        LocalDate c2EndDate = c2.getBoundedDate(endDate);
        LocalDate startD = c1StartDate;
        LocalDate endD = c2EndDate;

        if(c1StartDate.isBefore(c2StartDate))
            startD = c2StartDate;
        if(c1EndDate.isBefore(c2EndDate))
            endD = c1EndDate;

        if(startD.equals(endD) || startD.isAfter(endD) || endD.isBefore(startD))
            return 0;

        Company.PricesForDates pricesC1 = c1.getAdjClosePricesForPeriod(startD, endD);
        Company.PricesForDates pricesC2 = c2.getAdjClosePricesForPeriod(startD, endD);

        if(pricesC1.getPrices().size() != pricesC2.getPrices().size()) {
            System.out.println("saltato");
            return 0;
        }

        LinkedList<Float> pricesC1ROS = ROS(pricesC1.getPrices());
        LinkedList<Float> pricesC2ROS = ROS(pricesC2.getPrices());

        // Numerator
        float correlationNumerator = dotProduct(pricesC1ROS, pricesC2ROS) - dotProduct(pricesC1ROS) * dotProduct(pricesC2ROS);

        LinkedList<Float> pricesC1ROSSquared = powArray(pricesC1ROS, 2);
        LinkedList<Float> pricesC2ROSSquared = powArray(pricesC2ROS, 2);
        float term1 = dotProduct(subtractFromArray(pricesC1ROSSquared, (float) Math.pow(dotProduct(pricesC1ROS), 2)));
        float term2 = dotProduct(subtractFromArray(pricesC2ROSSquared, (float) Math.pow(dotProduct(pricesC2ROS), 2)));
//        float term1 = subtractFromArray(pricesC1ROSSquared, (float) Math.pow(dotProduct(pricesC1ROS), 2)).stream().reduce(0.0f, (tot, elem) -> tot += elem);
//        float term2 = subtractFromArray(pricesC2ROSSquared, (float) Math.pow(dotProduct(pricesC2ROS), 2)).stream().reduce(0.0f, (tot, elem) -> tot += elem);

        // Denominator
        double correlationDenominator = Math.sqrt(term1*term2);

        if(correlationDenominator == 0)
            return 0;

        return (correlationNumerator / correlationDenominator) / pricesC1ROS.size();
    }

    /**
     * Gets the Return Of the Stock for each element of the given prices by performing
     * ln({@code prices}[i]/{@code prices}[i - 1]) for each i.
     * It worth noting that the resulting array will have length prices.length - 1
     * @param prices    The prices of the stock
     * @return          The return of the stock for day t
     */
    static LinkedList<Float> ROS(LinkedList<Float> prices) {
        LinkedList<Float> ros = new LinkedList<>();

        for(int i = 1; i < prices.size(); i++) {
            ros.add((float) Math.log(prices.get(i)/prices.get(i - 1)));
        }

        return ros;
    }

    /**
     * Calculates the dot product of the given vector with its own
     * @param prices1   The vector
     * @return          The dot product of the vectors
     */
    static float dotProduct(LinkedList<Float> prices1) {
        return dotProduct(prices1, prices1);
    }

    /**
     * Calculates the dot product of the given vectors
     * @param prices1   The first vector
     * @param prices2   The second vector
     * @return          The dot product of the vectors
     */
    static float dotProduct(LinkedList<Float> prices1, LinkedList<Float> prices2) {
        float sum = 0.0f;
        for(int i = 0; i < prices1.size(); i++) {
            sum += prices1.get(i) * prices2.get(i);
        }

        return sum;
    }

    static LinkedList<Float> powArray(LinkedList<Float> array, int powExponent) {
        LinkedList<Float> pows = new LinkedList<>();

        for (Float item : array) {
            pows.add((float) Math.pow(item, powExponent));
        }

        return pows;
    }

    static LinkedList<Float> subtractFromArray(LinkedList<Float> array, float number) {
        LinkedList<Float> res = new LinkedList<>();

        for (Float item : array) {
            res.add(item - number);
        }

        return res;
    }
}
