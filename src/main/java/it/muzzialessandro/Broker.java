package it.muzzialessandro;

import it.muzzialessandro.manager.ErrorManager;
import it.muzzialessandro.manager.LogManager;
import it.muzzialessandro.utils.NetworkUtils;
import it.unipr.sowide.actodes.actor.Behavior;
import it.unipr.sowide.actodes.actor.CaseFactory;
import it.unipr.sowide.actodes.configuration.Configuration;
import it.unipr.sowide.actodes.controller.SpaceInfo;
import it.unipr.sowide.actodes.distribution.activemq.ActiveMqConnector;
import it.unipr.sowide.actodes.executor.active.ThreadCoordinator;
import it.unipr.sowide.actodes.executor.passive.OldScheduler;
import it.unipr.sowide.actodes.service.creation.Creator;
import it.unipr.sowide.actodes.service.logging.ConsoleWriter;
import it.unipr.sowide.actodes.service.logging.Logger;
import it.unipr.sowide.actodes.service.logging.util.NoCycleProcessing;

import java.net.SocketException;

public final class Broker extends Behavior
{
    private static final long serialVersionUID = 1L;

    @Override
    public void cases(CaseFactory caseFactory) { }

    public static void main(final String[] v) {
        Configuration conf =  SpaceInfo.INFO.getConfiguration();

        if(!conf.load(v)) {
            ErrorManager.raiseError(1, "Config file not found");
            return;
        }

        conf.setExecutor(new ThreadCoordinator());
        conf.setFilter(Logger.ACTIONS);
        conf.setLogFilter(new NoCycleProcessing());
        conf.addWriter(new ConsoleWriter());

        conf.addService(new Creator());

        final String brokerIP = conf.getString("broker_ip");
        LogManager.i("Starting ActoDES with ActiveMQ broker at " + brokerIP + ":61616");
        conf.setConnector(new ActiveMqConnector("tcp://" + brokerIP + ":61616", "persistent=false&useJmx=false"));

        conf.start();
    }
}
