package it.muzzialessandro.model;

import com.opencsv.bean.CsvBindByPosition;
import it.muzzialessandro.manager.LogManager;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Company implements Serializable {

    private static final long serialVersionUID = 1L;

    //region Constants

    private static final String FILE_EXT = ".csv";

    private static final String URL_BASE = "https://query1.finance.yahoo.com/v7/finance/download/";
    private static final String URL_FROM = "?period1=";
    private static final String URL_TO = "&period2=";
    private static final String URL_WITH_INTERVAL = "&interval=";
    private static final String URL_AND_PARAMS = "&events=history&includeAdjustedClose=true";

    //endregion

    //region Private Fields

    @CsvBindByPosition(position = 1)
    private String symbol;

    @CsvBindByPosition(position = 2)
    private String name;

    @CsvBindByPosition(position = 3)
    private String sector;

    @CsvBindByPosition(position = 4)
    private String industry;

    @CsvBindByPosition(position = 5)
    private String market;

    @CsvBindByPosition(position = 6)
    private String CIK;

    private LinkedList<PriceInstance> prices;

    private HashMap<String, HashMap<String, Double>> correlations;

    //endregion

    //region Getters and Setters

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getCIK() {
        return CIK;
    }

    public void setCIK(String CIK) {
        this.CIK = CIK;
    }

    public LinkedList<PriceInstance> getPrices() {
        return prices;
    }

    public void setPrices(LinkedList<PriceInstance> prices) {
        this.prices = prices;
    }

    public HashMap<String, HashMap<String, Double>> getCorrelations() {
        return correlations;
    }

    public void setCorrelations(HashMap<String, HashMap<String, Double>> correlations) {
        this.correlations = correlations;
    }

    //endregion

    /**
     * Retrieves prices from file, if exists, then updates them via the Yahoo Finance correspondence
     * @throws IOException
     * @throws ParseException
     */
    public void importPrices(String dataFolderPath, int startYear, int endYear, String dataFrequency) throws IOException, ParseException {
        String companyFilePath = dataFolderPath + this.symbol + FILE_EXT;
        LogManager.d("File path: "+ companyFilePath);

        Date fromDate = new GregorianCalendar(startYear, Calendar.JANUARY, 0).getTime();
        Date toDate = new GregorianCalendar(endYear, Calendar.JANUARY, 0).getTime();
        // if it needs an update
        if(fromDate.compareTo(toDate) < 0) {
            long fromDateLong = fromDate.getTime() / 1000;
            long toDateLong = toDate.getTime() / 1000;

            String compositeURL = URL_BASE +
                    this.symbol +
                    URL_FROM +
                    fromDateLong +
                    URL_TO +
                    toDateLong +
                    URL_WITH_INTERVAL +
                    dataFrequency +
                    URL_AND_PARAMS;

            LogManager.i("Downloading: " + compositeURL);

            FileUtils.copyURLToFile(
                    new URL(compositeURL),
                    new File(companyFilePath),
                    10000,
                    10000);

            BufferedReader reader = new BufferedReader(new FileReader(companyFilePath));
            importPricesFromReader(reader, null);
            reader.close();
        }

    }

    /**
     * Imports the prices from the given {@code reader}, updating a persistent storage using the given {@code writer}
     * @param reader            Input stream of data
     * @param writer            Output stream for updating persistent data
     * @throws IOException      Thrown by the reader/writer
     * @throws ParseException   Thrown by the data parser of the prices
     */
    public void importPricesFromReader(@NotNull BufferedReader reader, BufferedWriter writer) throws IOException, ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//        LogManager.d("Import prices");
        if(this.prices == null)
            this.prices = new LinkedList<>();

        String line = reader.readLine();
        // used to consume the first line (headers)
        if(writer != null && this.prices.isEmpty()) {
            writer.write(line);
            writer.newLine();
        }

        ZoneId localZone = ZoneId.systemDefault();
        while((line = reader.readLine()) != null){

            String[] items = line.split(",");
            try {
                this.prices.add(new PriceInstance(df.parse(items[0]).toInstant().atZone(localZone).toLocalDate(),
                        Float.parseFloat(items[1]),
                        Float.parseFloat(items[2]),
                        Float.parseFloat(items[3]),
                        Float.parseFloat(items[4]),
                        Float.parseFloat(items[5]),
                        (int)Float.parseFloat(items[6])));
            } catch (IndexOutOfBoundsException e1){
                // There is no price for this date, so I just add a null one
                this.prices.add(new PriceInstance(df.parse(items[0]).toInstant().atZone(localZone).toLocalDate(), 0, 0, 0, 0, 0, 0));
            } catch (Exception e){
                // ignore, just read till there are all the price components, then break
                LogManager.i("Error: " + e);
                break;
            }

            if(writer != null) {
                // write to file the retrieved file
                writer.write(line);
                writer.newLine();
            }
        }
    }

    @Override
    public String toString() {
        return "Company{" +
                "Symbol='" + symbol + '\'' +
                ", Name='" + name + '\'' +
                ", Sector='" + sector + '\'' +
                ", Industry='" + industry + '\'' +
                ", Market='" + market + '\'' +
                ", CIK='" + CIK + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Company))
            return false;

        return this.getSymbol().equals(((Company)obj).getSymbol());
    }

    /**
     * Gets the adjusted close prices between the given dates
     * @param startDate     The start date for the interested interval
     * @param endDate       The end date for the interested interval
     * @return              The list of the adjusted close prices between the given dates
     */
    public PricesForDates getAdjClosePricesForPeriod(LocalDate startDate, LocalDate endDate) {
        LinkedList<Float> prices = new LinkedList<>();

        int startIdx = getPriceIndexByDate(this.prices, startDate, true);
        int endIdx = getPriceIndexByDate(this.prices, endDate, false);

        if(startIdx == -1 || endIdx == -1)
            return null;

        for(int i = startIdx; i < endIdx; i++) {
            PriceInstance currPrice = this.prices.get(i);
            prices.add(currPrice.getAdjClose());
        }

        return new PricesForDates(prices, this.prices.get(startIdx).getDate(), this.prices.get(endIdx).getDate());
    }

    /**
     * Performs a boolean search to find the index of the {@code PriceInstance} corresponding to the given date
     * @param prices        The list of prices to iterate
     * @param date          The date to look for
     * @param isStartDate   True if the given date is a start date, False otherwise
     * @return              The index of the found {@code PriceInstance}, -1 if no match has been found
     */
    private int getPriceIndexByDate(@NotNull List<PriceInstance> prices, LocalDate date, boolean isStartDate) {
        return getPriceIndexByDate(prices, date, 0, isStartDate);
    }

    /**
     * Performs a boolean search to find the index of the {@code PriceInstance} corresponding to the given date
     * @param prices        The list of prices to iterate
     * @param date          The date to look for
     * @param itemBaseIdx   The base index for recursive passages, must be left 0 on external calls
     * @param isStartDate   True if the given date is a start date, False otherwise
     * @return              The index of the found {@code PriceInstance}, -1 if no match has been found
     */
    private int getPriceIndexByDate(@NotNull List<PriceInstance> prices, @NotNull LocalDate date, int itemBaseIdx, boolean isStartDate) {
        // recursion stop
        if(prices.size() == 0)
            return -1;

        int middleIdx = prices.size()/2;
        LocalDate middleDate = prices.get(middleIdx).getDate();

        if(middleDate.equals(date)) {
            return itemBaseIdx + middleIdx;
        }

        // recursion stop
        if(prices.size() == 1)
            return -1;

        int foundIdx;
        if(middleDate.isAfter(date)) {
            foundIdx = getPriceIndexByDate(prices.subList(0, middleIdx), date, itemBaseIdx, isStartDate);
            if(foundIdx != -1)
                return foundIdx;

            return getNearestDatePriceIndex(prices.subList(0, middleIdx), date, itemBaseIdx, isStartDate);
        }

        foundIdx = getPriceIndexByDate(prices.subList(middleIdx, prices.size()), date, middleIdx + itemBaseIdx, isStartDate);
        if(foundIdx != -1)
            return foundIdx;

        return getNearestDatePriceIndex(prices.subList(middleIdx, prices.size()), date, middleIdx + itemBaseIdx, isStartDate);
    }

    /**
     * Searches in the given prices list to find the nearest date to the given one
     * @param prices        The list to search on
     * @param date          The target date
     * @param startIdx      The index to add to the found one
     * @param isStartDate   Avoids result before if it is a start date and after if it is not (remember that is meant to find dates for a range)
     * @return              The index of the nearest date to the target one in the given list
     */
    private int getNearestDatePriceIndex(@NotNull List<PriceInstance> prices, @NotNull LocalDate date, int startIdx, boolean isStartDate) {
        long minFoundDistance = Integer.MAX_VALUE;
        int foundIdx = -1;
        for(int i = 0; i < prices.size(); i++) {
            LocalDate currDate = prices.get(i).getDate();
            // nearby date has to be not before the given date if it is a start date
            // analog reasoning for the end date
            if(isStartDate && currDate.isBefore(date) || !isStartDate && currDate.isAfter(date))
                continue;

            long foundDist = Math.abs(ChronoUnit.DAYS.between(currDate, date));
            if(foundDist < minFoundDistance) {
                minFoundDistance = foundDist;
                foundIdx = i;
            }
        }

        return startIdx + foundIdx;
    }

    /**
     * Checks if this company has prices for the given date, otherwise the closest date available is returned
     * @param date  The date to look for
     * @return      The given date if it is available, the closest one otherwise
     */
    public LocalDate getBoundedDate(LocalDate date) {
        if (date.isBefore(this.prices.getFirst().getDate()))
            return this.prices.getFirst().getDate();

        if (date.isAfter(this.prices.getLast().getDate()))
            return this.prices.getLast().getDate();

        return date;
    }

    public static class PricesForDates implements Serializable {
        LinkedList<Float> prices;
        LocalDate startDate;
        LocalDate endDate;

        public PricesForDates(LinkedList<Float> prices, LocalDate startDate, LocalDate endDate) {
            this.prices = prices;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public LinkedList<Float> getPrices() {
            return prices;
        }

        public void setPrices(LinkedList<Float> prices) {
            this.prices = prices;
        }

        public LocalDate getStartDate() {
            return startDate;
        }

        public void setStartDate(LocalDate startDate) {
            this.startDate = startDate;
        }

        public LocalDate getEndDate() {
            return endDate;
        }

        public void setEndDate(LocalDate endDate) {
            this.endDate = endDate;
        }
    }
}