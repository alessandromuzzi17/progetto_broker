package it.muzzialessandro.manager;

/**
 * Management class for errors
 */
public class ErrorManager {

    /**
     * Raise an error message exiting with the given code
     *
     * @param code  The error code
     * @param msg   The error message
     */
    public static void raiseError(int code, String msg) {
        ErrorManager.raiseError(code, msg, null);
    }

    /**
     * Raise an error message with the given exception stack trace end exits with the given code
     *
     * @param code  The error code
     * @param msg   The error message
     * @param e     The involved exception
     */
    public static void raiseError(int code, String msg, Exception e) {
        System.err.println("Error_" + code + ": " + msg);
        if(e != null)
            e.printStackTrace();

        System.exit(code);
    }

    /**
     * Raise a warning with the given code
     *
     * @param code  The warning code
     * @param msg   The warning message
     */
    public static void raiseWarning(int code, String msg) {
        raiseWarning(code, msg, null);
    }

    /**
     * Raise a warning with the given code printing the stack trace of the given exception
     *
     * @param code  The warning code
     * @param msg   The warning message
     * @param e     The involved exception
     */
    public static void raiseWarning(int code, String msg, Exception e) {
        // set yellow foreground
        System.out.print("\033[0;33m");
        System.out.print("Warning_" + code + ": " + msg);
        if (e != null)
            e.printStackTrace();

        // reset foreground color
        System.out.println("\033[0m");
    }
}
