package it.muzzialessandro.manager;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Management class for configuration files
 */
public class ConfigurationManager {

    private final Element configXMLRootNode;

    public ConfigurationManager(Element configXMLRootNode) {
        this.configXMLRootNode = configXMLRootNode;
    }

    //region Public Methods

    /**
     * Returns a ConfigurationManager provided with the provided configuration xml
     *
     * @param filePath      Configuration XML file path
     * @return              A ConfigurationManager provided with the provided configuration xml
     */
    public static @NotNull ConfigurationManager importConfigurationFromFile(String filePath) {
        Document configDoc = null;

        try {
            File fXmlFile = new File(filePath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            configDoc = dBuilder.parse(fXmlFile);
            configDoc.getDocumentElement().normalize();
        } catch (Exception e) {
            ErrorManager.raiseError(2, "The provided config file is not a valid XML file.", e);
        }

        assert configDoc != null;
        Element configXMLRootNode = configDoc.getDocumentElement();
        if (configXMLRootNode == null || !configXMLRootNode.getNodeName().equals("config")) {
            ErrorManager.raiseError(3, "The provided config file is not valid." +
                    "\nAccepted config files must have a 'config' containing all the required server parameters.");
        }

        return new ConfigurationManager(configXMLRootNode);
    }

    /**
     * Gives the requested property of config file
     *
     * @param fieldName     The property name
     * @return              The content of the requested property
     */
    public @NotNull String getFromConfig(@NotNull String fieldName) {
        Node node = this.configXMLRootNode.getElementsByTagName(fieldName).item(0);
        if(node == null)
            ErrorManager.raiseError(4, "Element '$fieldName' not found!");

        assert node != null;
        return node.getTextContent();
    }

    /**
     * Checks if the requested property is defined
     *
     * @param fieldName     The property name
     * @return              True if the property is defined, False otherwise
     */
    public boolean isDefined(@NotNull String fieldName) {
        return this.configXMLRootNode.getElementsByTagName(fieldName).item(0) != null;
    }

    //endregion
}
